# portfolio-adrian

### Project Overview
```
Tugas seleksi kelas web dev
```

### URL Setiap Page
- https://adrian-profile.netlify.app/ => Home

- https://adrian-profile.netlify.app/todo => Todo

- https://adrian-profile.netlify.app/_route_asal => Cek 404


## Project setup
```
npm install
```

### Local Development
```
npm run serve
```

